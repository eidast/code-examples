#! /usr/bin/env ruby


# Gems
require 'yaml'
require 'logger'
require 'csv'
require 'clamp'
require 'json'

shopify_csv = File.new("cache/shopify.csv", 'w+')

header = "Handle,Title,Body (HTML),Vendor,Type,Tags,Published,Option1 Name,Option1 Value,Option2 Name,Option2 Value,Option3 Name,Option3 Value,Variant SKU,Variant Grams,Variant Inventory Tracker,Variant Inventory Qty,Variant Inventory Policy,Variant Fulfillment Service,Variant Price,Variant Compare At Price,Variant Requires Shipping,Variant Taxable,Variant Barcode,Image Src,Image Position,Image Alt Text,Gift Card,Google Shopping / MPN,Google Shopping / Age Group,Google Shopping / Gender,Google Shopping / Google Product Category,SEO Title,SEO Description,Google Shopping / AdWords Grouping,Google Shopping / AdWords Labels,Google Shopping / Condition,Google Shopping / Custom Product,Google Shopping / Custom Label 0,Google Shopping / Custom Label 1,Google Shopping / Custom Label 2,Google Shopping / Custom Label 3,Google Shopping / Custom Label 4,Variant Image,Variant Weight Unit,Variant Tax Code\n"

#'Handle,Title,Body (HTML),Vendor,Type,Tags,Published,Option1 Name,Option1 Value,Option2 Name,Option2 Value,Option3 Name,Option3 Value,Variant SKU,Variant Grams,Variant Inventory Tracker,
# Variant Inventory Qty,Variant Inventory Policy,Variant Fulfillment Service,Variant Price,Variant Compare At Price,Variant Requires Shipping,Variant Taxable,Variant Barcode,Image Src,
# Image Position,Image Alt Text,Gift Card,Google Shopping / MPN,Google Shopping / Age Group,Google Shopping / Gender,Google Shopping / Google Product Category,SEO Title,SEO Description,
# Google Shopping / AdWords Grouping,Google Shopping / AdWords Labels,Google Shopping / Condition,Google Shopping / Custom Product,Google Shopping / Custom Label 0,Google Shopping / Custom Label 1,Google Shopping / Custom Label 2,Google Shopping / Custom Label 3,Google Shopping / Custom Label 4,Variant Image,Variant Weight Unit,Variant Tax Code\n'

shopify_csv.write(header)

Dir['data/*.json'].each do |json_file|
  data = JSON.parse(File.read(json_file)) # Parse JSON file to JSON structure

  line = "\"#{data['handle']}\",\"#{data['name']}\",\"#{data['description'].to_s.gsub(/\r/, '<BR/>')}\",,,\"#{data['tags'].to_s.gsub(/[\[\]"]/, '')}\",TRUE,,,,,,,#{data['id']},,,,deny,manual,\"#{data['price']}\",,,,,\"#{data['primary_image']['url']}\",,\"#{data['primary_image']['alt']}\"\n"
  shopify_csv.write(line)

  total_variants = data['variants'].count

  if total_variants > 0
    counter = 0
    case data['variants'][counter.to_s]['type']
      when 'option' # Variant is by dropdown menu
        data['variants'].each do |variant|
          line = "\"#{data['handle']}\",,,,,,TRUE,Title,\"#{data['variants'][counter.to_s]['name']}\",,,,,\"#{data['variants'][counter.to_s]['sku']}\",,,,deny,manual,\"#{data['variants'][counter.to_s]['price']}\",,,,,\"#{data['variants'][counter.to_s]['primary_image']['url']}\",,\"#{data['variants'][counter.to_s]['primary_image']['alt']}\"\n"
          counter += 1
          shopify_csv.write(line)
        end
      when 'color' # Variant is by color
        data['variants'].each do |variant|
          line = "\"#{data['handle']}\",,,,,,TRUE,Color,\"#{data['variants'][counter.to_s]['name']}\",,,,,\"#{data['variants'][counter.to_s]['sku']}\",,,,deny,manual,\"#{data['price']}\",,,,,\"#{data['variants'][counter.to_s]['primary_image']['url']}\",,\"#{data['variants'][counter.to_s]['primary_image']['alt']}\"\n"
          counter += 1
          shopify_csv.write(line)
        end
      when 'size' # Variant is by size
        data['variants'].each do |variant|
          line = "\"#{data['handle']}\",,,,,,TRUE,Size,\"#{data['variants'][counter.to_s]['name']}\",,,,,\"#{data['variants'][counter.to_s]['sku']}\",,,,deny,manual,\"#{data['price']}\",,,,,,,\n"
          counter += 1
          shopify_csv.write(line)
        end
    end
  end
end