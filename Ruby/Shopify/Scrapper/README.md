This crawler is used to get all product data from jefferspet.com

**_Troubleshooting_**

**C:/Ruby22-x64/lib/ruby/2.2.0/net/http.rb:923:in `connect': SSL_connect returned=
1 errno=0 state=SSLv3 read server certificate B: certificate verify failed (Open
SSL::SSL::SSLError)**

Download and save the newset certs (caperm.pem) from here:
https://curl.haxx.se/docs/caextract.html

Define the following enviroment variable
SSL_CERT_FILE=C:\somewhere\cacert.pem

**open-uri.rb:277:in `open_http': 403 Forbidden (OpenURI::HTTPError)**

Update or change your User-Agent in config.yml

_**Additional information:**_

**Expression used to clean scrape_href file:** 

`cat scrape_href | sed 's/\?via=.*//g' | uniq > scrape_href_clean`
