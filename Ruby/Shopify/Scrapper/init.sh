#!/bin/bash
# Script for initial creation of folders

mkdir -p cache/
mkdir -p data/
mkdir -p logs/
mkdir -p images/
