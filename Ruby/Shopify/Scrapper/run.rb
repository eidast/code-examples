# Gems required
require 'open-uri'
require 'nokogiri'
require 'yaml'
require 'openssl'
require 'logger'
require 'ostruct'
require 'httparty'
require 'csv'
require 'htmlentities'

=begin
def clean
  return
end
=end

# Return configuration object
#
# This method load a configuration file from a config folder
# Create the $LOG object for debugging proposes
# Return: config object
def start_run
  config = YAML.load_file('config/config.yml')

  $LOG = Logger.new("logs/#{config['app']['name']}-#{Date.today.to_s}.log", 'daily')

  return config
end

# Check if the index was scrapped if not proceed to load it.
#
# Return: index page for the this scrapper
def check_index_cache(filename, url)
  if !File.exists?("cache/#{filename}")
    index = Nokogiri::HTML(open(url, {"User-Agent" => "#{@config['browser']['userAgent']}"}))
    $LOG.info('URL loaded')
    file = File.new("cache/#{filename}", 'w+')
    file.write(index)
    file.close
  else
    file = File.open("cache/#{filename}", 'r')
    index = Nokogiri::HTML(file.read)
    file.close
    $LOG.info('Cache loaded')
  end

  return index
end

# Load the index and proceed to scrap it to generate the node search
#
# Return: Get all links to be scrapped
def load_index
  @config = start_run

  $LOG.info("Starting run at #{DateTime.now.to_s}")

  url = "#{@config['site']['protocol']}://#{@config['site']['domain']}/#{@config['site']['base']}"
  $LOG.info("Opening #{url}")
  @index = check_index_cache('horse_index.html', url)

  unless File.exist?('cache/node_href')
    nodes = File.open('cache/node_href', 'w+')

    @index.css('ul.nav-menu--is-selected li.menu-item a').each do |item|
      nodes.write("#{item.attr('href')}\n")
    end

  end

  return @index
end


def crawl_index
  unless File.exist?('cache/scrape_href')
    nodes = File.open('cache/node_href', 'r')
    scrape = File.new('cache/scrape_href', 'w+')

    nodes.each do |url|
      $LOG.info("Opening #{URI.parse(URI.encode(url.strip))}")
      offset = @config['site']['offset'].to_i
      begin
        while true
          $LOG.info("Opening: #{url.strip}?page=#{offset}")
          node = Nokogiri::HTML(open(URI.parse(URI.encode("#{url.strip}?page=#{offset}")), {"User-Agent" => "#{@config['browser']['userAgent']}"}))
          node.css('p.name a').each do |link|
            link_name = link.attr('href').to_s
            puts link_name
            scrape.write("#{link_name}\n")
          end
          offset += 1
          puts offset
          break if node.at('p:contains("No products found.")')
          break if node.css('div.top-controls.wl-browsing-controls').empty?
        end
      rescue
        puts "#{url.strip} can't be opened !!!"
        $LOG.error("#{url.strip} can't be opened !!!")
      end
    end
    scrape.close
  end
end

=begin
Variants

by Color
by Options
by Size

=end

def normalize_html(string)
  # Remove \n Lines
  # Replace TM Unicode for HTML entity &trade; \u2122
  # Replace Registered Unicode for HTML entity &reg; \u00AE
  # Replace 3/4 for HTML entity &frac34; \u00BE
  # Replace 1/2 for HTML entity &frac12; \u00BD
  # Replace 1/4 for HTML entity &frac14; \u00BC
  # Replace No Break Space for &nbsp; \u00A0

  coder = HTMLEntities.new

  coder.encode(string.to_s).gsub(/\u2122/, '&trade;').gsub(/\u00AE/, '&reg;').gsub(/\u00BE/, '&frac34;').gsub(/\u00BD/, '&frac12;').gsub(/\u00BC/, '&frac14;').gsub(/\u00A0/, '&nbsp;').gsub(/\n/, '')
end

# Scrap a URL to get all data required and save it on JSON file
def get_json_data
  if File.exist?('cache/scrape_href')
    products = File.open('cache/scrape_href', 'r')
    offset = 0

    products.each do |product|
      main_url = product.to_s.split('?')[0]
      url = product
      puts URI.parse(URI.encode(URI.decode("#{url.strip}")))

      begin
        page = Nokogiri::HTML(open(URI.parse(URI.encode(URI.decode("#{url.strip}"))), {"User-Agent" => "#{@config['browser']['userAgent']}"}))
      rescue OpenURI::HTTPError => e
        if e.message == '404 Not Found'
          $LOG.error("Opening: #{url.strip} 404 Not Found - skipping")
          next
        else
          raise e
        end
      end


      if page.css('p.unavailable').count == 1
        next
      end

      coder = HTMLEntities.new

      # Cleaning example coder.encode(page.css('p.brief-description').text.to_s).

      item = {
        metadata: {
          scrap_at: DateTime.now,
          scrapper: @config['app']['name'],
          scrapper_version: @config['app']['version'],
          url: url.strip
        },
        id: page.css('span[@itemprop="productID"]').text.slice(1..-1),
        handle: normalize_html(page.css('h1[@itemprop="name"]').text.to_s.gsub(/[^A-Za-z0-9\- ]/, '').gsub(/\s+/, '-').downcase),
        name: normalize_html(page.css('h1[@itemprop="name"]').text.to_s),
        short_description: normalize_html(page.css('p.brief-description').text),
        description: normalize_html(page.css('div[@itemprop="description"]').text.to_s),
        price: page.css('p.price meta[@itemprop="price"]').attr('content').text,
        attributes: {},
        variants: {},
        primary_image: {
            url: page.css('p.primary-image img').attr('src').text,
            alt: normalize_html(page.css('p.primary-image img').attr('alt').text)
        },
        tags: []
      }

      puts item[:id]

      # Check if the product exist locally then update tags

      if File.exists?("data/#{item[:id]}.json")
        puts "data/#{item[:id]}.json"
        json = File.read("data/#{item[:id]}.json")
        puts "######### FILE:"
        puts json
        item = JSON.parse(json, :quirks_mode => true)

        file = File.open("data/#{item['id']}.json", 'w+')

        puts item

        # Add tags

        page.css('div.wl-breadcrumbs h2.node,h3.node,h4.node,h5.node').each do |tag|
          item['tags'].push(tag.text.strip)
        end

        item['tags'] = item['tags'].uniq

        puts "######## VARIANTS:"
        puts item['variants']
        puts "######## TAGS:"
        puts item['tags']

        file.write(item.to_json)
        file.close
        puts 'Tags added skip to next URL ...'
        next
      end

      # Get all attributes

      page.css('div.grid-cell ul.attributes li').each do |attribute|
        attributes = attribute.content.split(':')
        item[:attributes].store(attributes[0].strip, normalize_html(attributes[1].strip))
      end

      # Variant by Options:
      # If exist a div.wl-value
      # Example: https://www.jefferspet.com/products/ultrashield-ex

      counter = 0
      page.css('div.wl-value select#sku option').each do |option|
        if option.text.include?('Please')
          next
        end
        sku = option.attr('value').strip
        variant = option.text.split("($")
        item[:variants][counter] = {}
        item[:variants][counter].store('type', 'option')
        item[:variants][counter].store('name', normalize_html(variant[0].to_s.strip))
        item[:variants][counter].store('price', variant[1].to_s.strip.chomp(')'))
        item[:variants][counter].store('sku', sku)

        temp_url = url.split('?')

        puts URI.parse(URI.encode(URI.decode("#{temp_url[0].strip}?sku=#{sku}")))
        temp_page = Nokogiri::HTML(open(URI.parse(URI.encode(URI.decode("#{temp_url[0].strip}?sku=#{sku}"))), {"User-Agent" => "#{@config['browser']['userAgent']}"}))

        item[:variants][counter][:primary_image] = {
            url: temp_page.css('p.primary-image img').attr('src').text,
            alt: temp_page.css('p.primary-image img').attr('alt').text
        }

        counter += 1
      end

      # Variant by Color:
      # If exist a div.colors
      # Example: https://www.jefferspet.com/products/horse-head-knee-high-socks-6-pair-pack

      counter = 0
      page.css('div.colors ul li.color img').each do |color|

        temp_url = url.split('?')
        puts URI.parse(URI.encode(URI.decode("#{temp_url[0].strip}?color=#{color.attr('alt').gsub(' ','+').strip}")))
        temp_page = Nokogiri::HTML(open(URI.parse(URI.encode(URI.decode("#{temp_url[0].strip}?color=#{color.attr('alt').gsub(' ','+').strip}"))), {"User-Agent" => "#{@config['browser']['userAgent']}"}))
        sku = temp_page.css('input#sku').first.attr('value')
        variant = color.attr('alt')
        item[:variants][counter] = {}
        item[:variants][counter].store('type', 'color')
        item[:variants][counter].store('name', variant)
        item[:variants][counter].store('sku', sku)

        item[:variants][counter][:primary_image] = {
            url: temp_page.css('p.primary-image img').attr('src').text,
            alt: temp_page.css('p.primary-image img').attr('alt').text
        }

        counter += 1
      end

      # Variant by Size:
      # If exist a div.sizes
      # Example: https://www.jefferspet.com/products/lettia-coolmax-flc-lined-girth-brn-size-44-in

      counter = 0
      page.css('div.sizes ul.button-group li').each do |size|
        #size.text

        #size.

        begin
          sku = size.css('label').attr('for').text.slice(4..-1)
        rescue NoMethodError => e
          next
        end

        variant = size.css('span').text

        item[:variants][counter] = {}
        item[:variants][counter].store('type', 'size')
        item[:variants][counter].store('name', variant)
        item[:variants][counter].store('sku', sku)

        counter += 1
      end

      # Create or Open a product

      if !File.exists?("data/#{item[:id]}.json")
        file = File.new("data/#{item[:id]}.json", 'w+')
        file.write(item.to_json)
        file.close
      end

      puts '######## DATA: '
      puts item.to_json
    end
  end
end

load_index
crawl_index
get_json_data